//--------------------------------------------------------------
// Generic Servo Expander
#include <Servo.h>
#include <Wire.h>
// My i2c Address
#define MYADDRESS 3
int i2cCommand = 0;
// Status
#define STATUS_LED 13 // Arduino built in LED
// Servo Bits
#define NBR_SERVOS 1
#define FIRST_SERVO_PIN 2
Servo Servos[NBR_SERVOS];
#define NEUTRAL 90
//--------------------------------------------------------------
void setup() {
  Serial.begin(57600); // DEBUG
  Serial.println("Stealth RC Servo Expander 1.1 - 1.31.14");
  pinMode(STATUS_LED, OUTPUT); // Enable Status LED Pin
  digitalWrite(STATUS_LED, LOW); // Turn it off
  Serial.print("Activating Servos");
  // Attach and center Servos
  for(int i =0; i < NBR_SERVOS; i++) {
    Serial.print(".");
    Servos[i].attach(FIRST_SERVO_PIN +i);
    Serial.print(".");
    Servos[i].write(NEUTRAL);
  }
  delay(500); // Wait a bit to make sure they're all centered
  // Detach from servo to safe power and stop jitter
  for(int i =0; i < NBR_SERVOS; i++) {
    Serial.print(".");
    Servos[i].detach();
  }
  Serial.println("");
  Serial.print("My i2c address:");
  Serial.println(MYADDRESS);
  Wire.begin(MYADDRESS); // Start I2C Bus as a Slave
  Wire.onReceive(receiveEvent); // routine we call when we get a commandregister event
  i2cCommand = -1; // Make sure i2cCommand isn't set
  Serial.println("Listening for i2c Command");
}
//--------------------------------------------------------------
// receive Event
// this is called every time we detect an i2c command for us
//--------------------------------------------------------------
void receiveEvent(int howMany) {
  i2cCommand = Wire.read(); // receive byte as an integer
  Serial.print("i2c Command = ");
  Serial.println(i2cCommand);
}
//--------------------------------------------------------------
// Perform this loop indefinitely
//--------------------------------------------------------------
void loop() {
  if (i2cCommand == 0) {
    digitalWrite(STATUS_LED, LOW);
    i2cCommand=-1;
  }
  //--------------------------------------------------------------
  // Status / Reset from Main Controller
  if (i2cCommand == 1) {
    // "1" is a special startup Stealth RC command.
    // It tells us the main Controller Receiver reset.
    // Typically we've just rebooted/reset too.
    // Status LED is set on to show we got the reset message
    Serial.println("Got reset message");
    // Turn on Status LED so we can visually see that we got a reset command
    digitalWrite(STATUS_LED, HIGH);
    // Always reset i2cCommand to -1 or command will loop/repeat forever
    i2cCommand=-1;
  }
  //--------------------------------------------------------------
  // Command 1
  if (i2cCommand==2) {
    Serial.println("COMMAND 2!");
    // Turn on Status LED to show we're in the command
    digitalWrite(STATUS_LED, HIGH);
    // Always reset i2cCommand to -1 or command will loop/repeat forever
    i2cCommand=-1;
    // Turn off Status LED
    delay(500);
    digitalWrite(STATUS_LED, LOW);
  }
}
