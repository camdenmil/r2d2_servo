#include <Servo.h>

#define LINE_BUF_SIZE 128   //Maximum input string length
#define ARG_BUF_SIZE 64     //Maximum argument string length
#define MAX_NUM_ARGS 3      //Maximum number of arguments

bool error_flag = false;
char line[LINE_BUF_SIZE];
char args[MAX_NUM_ARGS][ARG_BUF_SIZE];

#define N_SERVOS 8
#define FIRST_SERVO 2
Servo servos[N_SERVOS];
#define NEUTRAL 90

void move_servo(int servo, int position){
  servos[servo].write(position);
  Serial.print("Moving servo ");
  Serial.print(servo);
  Serial.print(" to ");
  Serial.print(position);
  Serial.print('\n');
  
}

//Function declarations
int cmd_servo();
 
//List of functions pointers corresponding to each command
int (*commands_func[])(){
    &cmd_servo,
};
 
//List of command names
const char *commands_str[] = {
    "servo",
};
 
int num_commands = sizeof(commands_str) / sizeof(char *);

void setup() {
    Serial.begin(9600);
    pinMode(13, OUTPUT);
    Serial.println("Servo sweep test");
    for(int i=0;i<N_SERVOS;i++){
      servos[i].attach(FIRST_SERVO+i);
      servos[i].write(NEUTRAL);
    }
}
 
void loop() {
    my_cli();
}

int cmd_servo(){
  int servo = atoi(args[1]);
  if(servo < N_SERVOS){
    int pos = atoi(args[2]);
    if(pos < 180 && pos > 0){
      move_servo(servo, pos);
    }
  }
}

void my_cli(){
    Serial.print("> ");
 
    read_line();
    if(!error_flag){
        parse_line();
    }
    if(!error_flag){
        execute();
    }
 
    memset(line, 0, LINE_BUF_SIZE);
    memset(args, 0, sizeof(args[0][0]) * MAX_NUM_ARGS * ARG_BUF_SIZE);
 
    error_flag = false;
}

void read_line(){
    String line_string;
 
    while(!Serial.available());
 
    if(Serial.available()){
        line_string = Serial.readStringUntil('\n');
        if(line_string.length() < LINE_BUF_SIZE){
          line_string.toCharArray(line, LINE_BUF_SIZE);
          Serial.println(line_string);
        }
        else{
          Serial.println("Input string too long.");
          error_flag = true;
        }
    }
}
 
void parse_line(){
    char *argument;
    int counter = 0;
 
    argument = strtok(line, " ");
 
    while((argument != NULL)){
        if(counter < MAX_NUM_ARGS){
            if(strlen(argument) < ARG_BUF_SIZE){
                strcpy(args[counter],argument);
                argument = strtok(NULL, " ");
                counter++;
            }
            else{
                Serial.println("Input string too long.");
                error_flag = true;
                break;
            }
        }
        else{
            break;
        }
    }
}
 
int execute(){  
    for(int i=0; i<num_commands; i++){
        if(strcmp(args[0], commands_str[i]) == 0){
            return(*commands_func[i])();
        }
    }
 
    Serial.println("Invalid command. Type \"help\" for more.");
    return 0;
}
