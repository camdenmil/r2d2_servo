#include <Servo.h>

#define N_SERVOS 2
#define FIRST_SERVO 2
Servo servos[N_SERVOS];
#define NEUTRAL 90

void move_servo(int servo, int position){
  servos[servo].write(position);
  Serial.print("Moving servo ");
  Serial.print(servo);
  Serial.print(" to ");
  Serial.print(position);
  Serial.print('\n');
  
}

void setup() {
  Serial.begin(115200);
  Serial.println("Servo sweep test");
  for(int i=0;i<N_SERVOS;i++){
    servos[i].attach(FIRST_SERVO+i);
    servos[i].write(NEUTRAL);
  }
}

void loop() {
  for(int i=0;i<N_SERVOS;i++){
    move_servo(i, 45);
    delay(1000);
    move_servo(i, 135);
    delay(1000);
    move_servo(i, NEUTRAL);
  }
}
